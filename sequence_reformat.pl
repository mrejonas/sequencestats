#!/usr/bin/perl
use warnings ;
use Bio::SeqIO ;

my $seqio = Bio::SeqIO->new(-file=>$ARGV[0], -format=>'fasta') ;
while (my $seq=$seqio->next_seq()){
	# Get sequence id, sequence and sequence length
	my $id = $seq->display_id ; 	my $sequence = $seq->seq ;
	my $sequence_length = $seq->length ;
	print "$id $sequence_length $sequence\n" ;
}