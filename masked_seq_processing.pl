#!/usr/bin/perl
use warnings ;
use Bio::SeqIO ;

my $seqio = Bio::SeqIO->new(-file=>$ARGV[0], -format=>'fasta') ;
open(GFF, ">>repeat_locations.gff3") ;
open(STATS,">>contig_stats.csv") ;
while (my $seq=$seqio->next_seq()){
	my $id = $seq->display_id ;
	my $sequence = $seq->seq ;
	my $sequence_length = my $iteration = $seq->length ;
	$A_count = $G_count = $C_count  = $T_count = $N_count = 0 ;
	my $region_start = my $region_end = 0 ;
	my $initiate_hardmasking = $initiate_softmasking = 0 ;
	while($iteration > 0){
		my $base = chop($sequence) ;
		if($base eq "A"){
			$A_count++;
			if($initiate_hardmasking ==1){
				$initiate_hardmasking = 0 ;	$region_end = $iteration ;
				my $region_length = $region_start - $region_end ;
				print GFF "$id\tRepeatMasker\trepeat\t$region_end\t$region_start\t.\t+\t.\tID=UNK$region_length\n" ;
#				print "$id\t$region_end\t$region_start\t$region_length\n" ;
			}
			}
		if($base eq "G"){
			$G_count++;
			if($initiate_hardmasking ==1){
				$initiate_hardmasking = 0 ;	$region_end = $iteration ;
				my $region_length = $region_start - $region_end ;
				print GFF "$id\tRepeatMasker\trepeat\t$region_end\t$region_start\t.\t+\t.\tID=UNK$region_length\n" ;
#				print "$id\t$region_end\t$region_start\t$region_length\n" ;
				}
			}
		if($base eq "C"){
			$C_count++;
			if($initiate_hardmasking ==1){
				$initiate_hardmasking = 0 ;	$region_end = $iteration ;
				my $region_length = $region_start - $region_end ;
				print GFF "$id\tRepeatMasker\trepeat\t$region_end\t$region_start\t.\t+\t.\tID=UNK$region_length\n" ;
#				print "$id\t$region_end\t$region_start\t$region_length\n" ;
			}
			}
		if($base eq "T"){
			$T_count++;
			if($initiate_hardmasking ==1){
				$initiate_hardmasking = 0 ;	$region_end = $iteration ;
				my $region_length = $region_start - $region_end ;
				print GFF "$id\tRepeatMasker\trepeat\t$region_end\t$region_start\t.\t+\t.\tID=UNK$region_length\n" ;
#				print "$id\t$region_end\t$region_start\t$region_length\n" ;
			}
			}

		if($base eq "N"){
			$N_count++;
			if($initiate_hardmasking ==0){
			$initiate_hardmasking = 1 ;	$region_start = $iteration ;
		}
			}
	$iteration-- ;
	}
	$A_perc=100*($A_count/$sequence_length) ;
	$G_perc=100*($G_count/$sequence_length );
	$C_perc=100*($C_count/$sequence_length );
	$T_perc=100*($T_count/$sequence_length) ;
	$N_perc=100*($N_count/$sequence_length) ;
	printf STATS "$id\t$sequence_length\tA:  %5.2f($A_count)\tG: %5.2f($G_count)\tC:  %5.2f($C_count)\tT:  %5.2f($T_count)\tN:  %5.2f($N_count)\n",$A_perc,$G_perc,$C_perc,$T_perc, $N_perc;
}