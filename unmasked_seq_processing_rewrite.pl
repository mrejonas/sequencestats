#!/usr/bin/perl
use warnings ;
use Bio::SeqIO ;

my $seqio = Bio::SeqIO->new(-file=>$ARGV[0], -format=>'fasta') ;
open(GFF, ">>repeat_locations.gff3") ;
open(STATS,">>contig_stats.csv") ;
while (my $seq=$seqio->next_seq()){
	# Get sequence id, sequence and sequence length
	my $id = $seq->display_id ; 	my $sequence = $seq->seq ;
	my $sequence_length = $seq->length ;
	$sequence = reverse($sequence) ;

	# Count all bases, including "N", excluding softmasked bases
	$A_count = $G_count = $C_count  = $T_count = $N_count = 0 ;
	# Softmasked bases are lower-cased; count them
	$lca_count = $lcg_count = $lcc_count  = $lct_count = 0 ;

	# Initiate region limits
	my $region_start = my $region_end = 0 ;
	my $initiate_hardmasking = $initiate_softmasking = 0 ;
	for(my $idx = 1; $idx <=  $sequence_length ; $idx++){
		my $base = chop($sequence) ;

		if($base eq lc($base)){
			if($initiate_softmasking ==0){
				$initiate_softmasking = 1 ;
				$region_start = $idx ;
			}
			if($initiate_hardmasking ==1){
				$initiate_hardmasking = 0 ;
				$region_end = $idx - 1 ;
			}
		}

		if($base eq "N"){
			$N_count++;
			if($initiate_hardmasking ==0){
				$initiate_hardmasking = 1 ;
				$region_start = $idx ;
			}
			if($initiate_softmasking ==1){
				$initiate_softmasking = 0 ;
				$region_end = $idx - 1 ;
			}

		}

		if($base eq "A"){
			$A_count++;
			if($initiate_hardmasking ==1){
				$initiate_hardmasking = 0 ;	$region_end = $idx - 1 ;
				my $region_length = $region_end - $region_start ;
				print GFF "$id\tRepeatMasker\trepeat\t$region_start\t$region_end\t.\t+\t.\tID=UNK$region_length\n" ;
#				print "$id\t$region_start\t$region_end\t$region_length\n" ;
			}
			}elsif($base eq "a"){
			$lca_count++;
			if($initiate_softmasking ==1){
				$initiate_softmasking = 0 ;	$region_end = $idx ;
				my $region_length = $region_end - $region_start ;
				print GFF "$id\tDust\trepeat\t$region_start\t$region_end\t.\t+\t.\tID=UNK$region_length\n" ;
#				print "$id\t$region_start\t$region_end\t$region_length\n" ;
			}
			}
		if($base eq "G"){
			$G_count++;
			if($initiate_hardmasking ==1){
				$initiate_hardmasking = 0 ;	$region_end = $idx ;
				my $region_length = $region_end - $region_start ;
				print GFF "$id\tRepeatMasker\trepeat\t$region_start\t$region_end\t.\t+\t.\tID=UNK$region_length\n" ;
#				print "$id\t$region_start\t$region_end\t$region_length\n" ;
				}
			}elsif($base eq "g"){
			$lcg_count++;
			if($initiate_softmasking ==1){
				$initiate_softmasking = 0 ;	$region_end = $idx ;
				my $region_length = $region_end - $region_start ;
				print GFF "$id\tDust\trepeat\t$region_start\t$region_end\t.\t+\t.\tID=UNK$region_length\n" ;
#				print "$id\t$region_start\t$region_end\t$region_length\n" ;
			}
			}
		if($base eq "C"){
			$C_count++;
			if($initiate_hardmasking ==1){
				$initiate_hardmasking = 0 ;	$region_end = $idx ;
				my $region_length = $region_end - $region_start ;
				print GFF "$id\tRepeatMasker\trepeat\t$region_start\t$region_end\t.\t+\t.\tID=UNK$region_length\n" ;
#				print "$id\t$region_start\t$region_end\t$region_length\n" ;
			}
			}elsif($base eq "c"){
			$lcc_count++;
			if($initiate_softmasking ==1){
				$initiate_softmasking = 0 ;	$region_end = $idx ;
				my $region_length = $region_end - $region_start ;
				print GFF "$id\tDust\trepeat\t$region_start\t$region_end\t.\t+\t.\tID=UNK$region_length\n" ;
#				print "$id\t$region_start\t$region_end\t$region_length\n" ;
			}
			}
		if($base eq "T"){
			$T_count++;
			if($initiate_hardmasking ==1){
				$initiate_hardmasking = 0 ;	$region_end = $idx ;
				my $region_length = $region_end - $region_start ;
				print GFF "$id\tRepeatMasker\trepeat\t$region_start\t$region_end\t.\t+\t.\tID=UNK$region_length\n" ;
#				print "$id\t$region_start\t$region_end\t$region_length\n" ;
			}
			}elsif($base eq "t"){
			$lct_count++;
			if($initiate_softmasking ==1){
				$initiate_softmasking = 0 ;	$region_end = $idx ;
				my $region_length = $region_end - $region_start ;
				print GFF "$id\tDust\trepeat\t$region_start\t$region_end\t.\t+\t.\tID=UNK$region_length\n" ;
#				print "$id\t$region_start\t$region_end\t$region_length\n" ;
			}
			}
	}
	$A_perc=100*($A_count/$sequence_length) ;
	$G_perc=100*($G_count/$sequence_length );
	$C_perc=100*($C_count/$sequence_length );
	$T_perc=100*($T_count/$sequence_length) ;
	$N_perc=100*($N_count/$sequence_length) ;
	$lca_perc=100*($lca_count/$sequence_length) ;
	$lcg_perc=100*($lcg_count/$sequence_length );
	$lcc_perc=100*($lcc_count/$sequence_length );
	$lct_perc=100*($lct_count/$sequence_length) ;
	printf STATS "$id\t$sequence_length A:  %5.2f($A_count) G: %5.2f($G_count) 
				C:  %5.2f($C_count) T:  %5.2f($T_count) N:  %5.2f($N_count) 
				a:  %5.2f($lca_count) g: %5.2f($lcg_count) C:  %5.2f($lcc_count)   
				t:  %5.2f($lct_count)\n", $A_perc, $G_perc, $C_perc, 
				$T_perc, $N_perc, $lca_perc, $lcg_perc, $lcc_perc, $lct_perc;
}