#!/usr/bin/PYTHON
#inputfile = open('../Seabass_perl/RNA_masked_reformatted.txt', 'r')
inputfile = open('../Seabass_perl/RNA_unmasked_reformatted.txt', 'r')
print "Masked Contig Stats"
for line in inputfile:
    field = line.split()
    N = field[2].count("N")
    A = field[2].count("A")
    G = field[2].count("G")
    C = field[2].count("C")
    T = field[2].count("T")
    a = field[2].count("a")
    g = field[2].count("g")
    c = field[2].count("c")
    t = field[2].count("t")
       
    print field[0], " ", field[1], " ", "N:", N, "A:", A, "G:", G, "C:", C, "T:", T, "a:", a, "g:", g, "c:", c, "t:", t